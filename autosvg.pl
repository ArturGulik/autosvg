#!/bin/env perl
#              _
#   __ _ _   _| |_ ___  _____   ____ _
#  / _` | | | | __/ _ \/ __\ \ / / _` |
# | (_| | |_| | || (_) \__ \\ V / (_| |
#  \__,_|\__,_|\__\___/|___/ \_/ \__, |
#                                |___/
#
# Copyright (C) 2022-2023 Artur Gulik
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

use strict;
use warnings;
use feature "say";

use Data::Dumper;
use XML::LibXML;
use XML::LibXML::XPathContext;

my $MAJOR   = 1;
my $MINOR   = 0;
my $VERSION = "$MAJOR.$MINOR";
my $YEARS   = "2022-2023";

# Path variables
my $defaultConfigPath = "autosvg.conf";
my $configPath        = undef;
if ( -f $defaultConfigPath ) {
  $configPath = $defaultConfigPath;
}
my $stepsPath;

# Config variables
my $warnings      = 1;
my $dieOnWarnings = 0;
my $verbose       = 0;

# XML processing variables
my @userColumnNames;

# @dbData stores the data after running the "data" verb action
# every entry is a hash for one record from the database.
# Hash example: (userColumnName => dataInThatColumn).
# This array is meant to be accessed on the top level only.
# Minor verbs should use %userData, a hash containing one single entry
# from @dbData, set by the top level function before runnning steps
# for a particular entry
my @dbData;
my %userData;
my %userDataUsed;

my @steps;
my $s = 0;    # index of currently processed step;

# currently created file (one for one data userData)
my $document;
my $xpc;

my $tellUserGlobIndent = 0;

# a hash of verbs with depth (that use code blocks ending with 'done')
my %deepVerbs = (
  'process' => 1,
  'if'      => 1
);

#describes whether process should clear the document before each iteration
my $clear;

sub copyright {
  say "autosvg $VERSION";
  say "Copyright (C) $YEARS Artur Gulik.";
  say "License: GNU GPL version 3 "
    . "<https://www.gnu.org/licenses/gpl-3.0.html>";
  say "This is free software: you are free to change and redistribute it.";
  say "There is NO WARRANTY, to the extent permitted by law.";
}

sub help {
  say "Usage:\r\n  autosvg [options]\n";
  say "Options:";
  say "  -h, --help\t\t\tDisplay this help information and exit";
  say "  -V, --version\t\t\tDisplay version information and exit";
  say "  -s, --steps=STEPS\t\tSpecify the steps file to load";
  say "  -c, --config=CONFIG\t\tSpecify a configuration file to load";
  say "  -v, --verbose\t\t\tEnable verbose mode (output more information)";
  say "  -w, --warn\t\t\tEnable warnings";
  say
"  -W, --die-on-warnings\t\tExit the program on any warning, implies --warn";
  say
"  --, --end-of-options \t\tMark the end of options. Process every following option as a regular argument";
}

sub tellUser {
  my $message     = shift;
  my $indentation = shift;
  my $symbol      = shift;
  $symbol      = "│" if ( !defined $symbol );
  $indentation = 0   if ( !defined $indentation );
  $indentation += $tellUserGlobIndent;
  print " " x $indentation . $symbol if ( $verbose and $indentation );
  say $message                       if $verbose;
}

sub warnUser {
  my $message = shift;
  die $message . "Stopped on warning.\n" if $dieOnWarnings;
  say $message                           if $warnings;
}

sub setConfigPath {
  $configPath = shift;
  if ( !-f $configPath ) {
    die "autosvg: Configuration file not found: $configPath\n";
  }
}

sub loadConfig {
  if ( -f $configPath ) {
    loadSteps($configPath);
    runCurrentSteps();
  }
  else {
    die
"autosvg: Provided config path does not lead to an accessible plain file: $configPath";
  }
}

sub setStepsPath {
  $stepsPath = shift;
  if ( !-f $stepsPath ) {
    die "autosvg: Steps file not found: $stepsPath\n";
  }
}

sub loadSteps {
  my $path = shift;
  $path = $stepsPath if ( !defined $path );
  die "autosvg: This version requires using a 'steps' file. '$path' "
    . "was not found\n"
    unless ( -f $path );

  open my $stepsHandle, $path
    or die "autosvg: Steps could not be read: $path:\n$!\n";

  # Overwrite previous steps
  @steps = ();

  while ( my $line = <$stepsHandle> ) {
    push @steps, $line;
  }
  close $stepsHandle;
}

sub expression {
  my $e = join ' ', @_;

  # Process newlines
  $e =~ s/(?<![\\])[\\][n]/\n/g;

  my $error;

  # Swap variable references with their values
  $e =~ s/((?<![\\])[{])(.*?)(?<![\\])[}]/
    "if (defined \$userData{'$2'}) {
      \$userDataUsed{'$2'} = 1;
      return \"
      return \\\"\\\$userData{'$2'}\\\";
      \";
    }
return \"\\\$error = '$2';\";"/eeeg;

  die "autosvg: expression: " . "User variable not found: $error\n"
    if ( defined $error );

  # Process escaped curly braces
  $e =~ s/(?<![\\])[\\]([{]|[}])/$1/g;

  # Swap system commands with their outputs
  $e =~ s/((?<![\\])[`])(.*?)(?<![\\])[`]/`$2`/eg;

  # Process escaped backticks (outside of matched run blocks)
  $e =~ s/(?<![\\])[\\][`]/\`/g;

  # Process escaped backticks
  $e =~ s/[\\][\\]/\\/g;

  return $e;
}

# Parsing command line options
my $argc          = 0;
my $noMoreOptions = 0;
$userData{0}     = join ' ', @ARGV;
$userDataUsed{0} = 1;
for ( my $i = 0 ; $i < @ARGV ; $i++ ) {
  my $string = $ARGV[$i];
  my @chars  = split "", $string;

  if ( $noMoreOptions or $chars[0] ne "-" ) {
    $userData{ $argc + 1 }     = $ARGV[$i];
    $userDataUsed{ $argc + 1 } = 0;
    $argc++;
    next;
  }
  if ( $string eq "--" or $string eq "--end-of-options" ) {
    $noMoreOptions = 1;
    next;
  }
  for ( my $j = 1 ; $j < @chars ; $j++ ) {
    if ( $chars[$j] eq "h"
      || $ARGV[$i] eq "--help" )
    {
      help();
      exit;
    }
    if ( $chars[$j] eq "V"
      || $ARGV[$i] eq "--version" )
    {
      copyright();
      exit;
    }
    if ( $chars[$j] eq "c"
      || $ARGV[$i] =~ s/^--config=/-c=/
      || $ARGV[$i] =~ s/^--config$/-c/ )
    {
      my $cmd = join( '', @chars );
      $cmd = "-c" if ( $cmd !~ s/--config.*/--config/ );
      die "autosvg: Option " . $cmd . " requires an argument\n"
        if ( $i == @ARGV - 1 && ( $ARGV[$i] !~ m/^-c=/ ) );
      my $path;
      if ( $ARGV[$i] =~ m/^-c=/ ) {
        $path = substr $ARGV[$i], 3, ( length( $ARGV[$i] ) - 3 );
      }
      else {
        $path = $ARGV[ $i + 1 ];
      }
      $i++;
      setConfigPath($path);

      #loadConfig();
      last;
    }
    if ( $chars[$j] eq "s"
      || $ARGV[$i] =~ s/^--steps=/-s=/
      || $ARGV[$i] =~ s/^--steps$/-s/ )
    {
      my $cmd = join( '', @chars );
      $cmd = "-s" if ( $cmd !~ s/--steps.*/--steps/ );
      die "autosvg: Option " . $cmd . " requires an argument\n"
        if ( $i == @ARGV - 1 && ( $ARGV[$i] !~ m/^-s=/ ) );
      my $path;
      if ( $ARGV[$i] =~ m/^-s=/ ) {
        $path = substr $ARGV[$i], 3, ( length( $ARGV[$i] ) - 3 );
      }
      else {
        $path = $ARGV[ $i + 1 ];
      }
      $i++;
      setStepsPath($path);

      #loadSteps();
      last;
    }
    if ( $chars[$j] eq "v"
      || $ARGV[$i] eq "--verbose" )
    {
      $verbose = 1;
      last if $ARGV[$i] eq "--verbose";
      next;
    }
    if ( $chars[$j] eq "w"
      || $ARGV[$i] eq "--warn" )
    {
      $warnings = 1;
      last if $ARGV[$i] eq "--warn";
      next;
    }
    if ( $chars[$j] eq "W"
      || $ARGV[$i] eq "--die-on-warnings" )
    {
      $warnings      = 1;
      $dieOnWarnings = 1;
      last if $ARGV[$i] eq "--die-on-warnings";
      next;
    }
    die "autosvg: Unrecognized option: $ARGV[$i]\n"
      if ( $chars[$j] eq "-" );
    die "autosvg: Unrecognized option: -$chars[$j]\n";
  }
  next;
}
loadConfig() if ( defined $configPath );
if ( defined $stepsPath ) {
  loadSteps();
}
else {
  $stepsPath = "steps";
  loadSteps();
}

# todo: check if no ID's are used multiple times
sub updateDocument {
  $document = shift;

  $xpc = XML::LibXML::XPathContext->new($document);
  $xpc->registerNs( x => 'http://www.w3.org/2000/svg' );
}

sub evalStep {
  my $trying    = 0;
  my $wholeStep = shift;
  my @step      = split( " ", $wholeStep );
  tellUser("@step");
  my $command = shift @step;
  return if ( !defined $command );
  if ( substr( $command, 0, 1 ) eq '#' ) {
    tellUser( "Comment recognized, skipping", 1, " " );
    return;
  }
  if ( $command eq "try" ) {
    $trying  = 1;
    $command = shift @step;
  }
  if ( $command eq "exit" ) {
    tellUser( "Exiting the program", 1, " " );
    $s = @steps;
    $userDataUsed{"evalOutput"} = 1;
    foreach my $key ( keys %userData ) {
      if ( !$userDataUsed{$key} ) {
        warnUser( "User variable " . $key . " was never used!" );
      }
    }
    return;
  }
  if ( $command eq "shell" ) {
    my $text = expression( join ' ', @step );
    system $text;
    return;
  }
  if ( $command eq "eval" ) {
    my $var = "evalOutput";
    if ( defined $step[-2] and $step[-2] eq "to" ) {
      $var = expression( pop @step );
      pop @step;    # get rid of the "to" word
    }
    elsif ( defined $step[-2] and $step[-2] eq "\to" ) {
      $step[-2] = "to";
    }
    $tellUserGlobIndent++;
    my $code = expression( join ' ', @step );
    tellUser( "Evaluating: $code", 1, " " );
    my $output = eval $code;
    die "autosvg: eval: Code evaluation failed.\n$@\n" if $@;
    if ( !defined $output ) {
      tellUser( "Evaluation succeeded with no output", 1, " " );
      evalStep("my $var ''");
      $tellUserGlobIndent--;
      return;
    }
    tellUser( "Received output: $output", 1, " " );
    evalStep("my $var $output");
    $tellUserGlobIndent--;
    return $output;
  }
  if ( $command eq "add" ) {
    die "No file path given to 'add'" unless defined $step[0];
    my $location;
    my $elementId;
    if ( defined $step[1] and $step[1] eq "from" ) {
      $elementId = expression( shift @step );
      shift @step;    # Omit the 'from' keyword
      $location = expression( shift @step );
    }
    else {
      $location = expression( $step[0] );
    }

    if ( defined $elementId ) {
      tellUser( "Adding the element with id '$elementId' from file '$location'",
        1, " " );
    }
    else {
      tellUser( "Adding file '$location'", 1, " " );
    }

    my $sourceFile = XML::LibXML->load_xml( location => $location );

    if ( not defined $elementId and not defined $document ) {
      updateDocument($sourceFile);
      return;
    }

    my $tempXpc = XML::LibXML::XPathContext->new($sourceFile);
    $tempXpc->registerNs( x => 'http://www.w3.org/2000/svg' );

    my $toAdd;
    if ( defined $elementId ) {
      ($toAdd) = $tempXpc->findnodes( '//x:*[@id="' . "$elementId" . '"]' );
      die "autosvg: add: No nodes with the specified id found: "
        . "$elementId\n"
        if ( !defined $toAdd );

      if ( !defined $document ) {
        my $newDocument = XML::LibXML::Document->createDocument();
        $newDocument->setDocumentElement($newDocument->createElement('svg'));
        $newDocument->documentElement->appendChild($toAdd);
        updateDocument($newDocument);
        return;
      }
    }
    else {
      # find svg elements in the "x" namespace
      ($toAdd) = $tempXpc->findnodes('//x:svg');
      die "autosvg: add: Incorrect svg file '$location': svg element not found"
        unless defined $toAdd;
    }

    my ($insertLocation) = $xpc->findnodes('//x:svg');
    $insertLocation->appendChild($toAdd);
    return;
  }
  if ( $command eq "write" ) {
    my $id     = expression( shift @step );
    my $value  = expression( join ' ', @step );
    my ($node) = $xpc->findnodes( '//x:*[@id="' . "$id" . '"]' );
    if ( !defined $node ) {
      return if ($trying);
      die "autosvg: write: No nodes with the specified id found: $id\n";
    }

    tellUser( "Writing text '$value' to the element with id '$id'", 1, " " );

    bless $node, "XML::LibXML::Text";

    $node->setData($value);
    return;
  }
  if ( $command eq "xml" ) {
    my $id     = expression( shift @step );
    my $value  = expression( join ' ', @step );
    my ($node) = $xpc->findnodes( '//x:*[@id="' . "$id" . '"]' );
    if ( !defined $node ) {
      return if ($trying);
      die "autosvg: xml: No nodes with the specified id found: $id\n";
    }

    tellUser( "Writing xml '$value' to the element with id '$id'", 1, " " );

    bless $node, "XML::LibXML::Text";
    $node->setData("");

    bless $node, "XML::LibXML::Element";
    $node->appendWellBalancedChunk($value);
    return;
  }
  if ( $command eq "set" ) {
    my $id   = expression( shift @step );
    my $attr = expression( shift @step );

    my ($node) = $xpc->findnodes( '//x:*[@id="' . "$id" . '"]' );
    die "autosvg: set: No nodes with the specified id found: $id\n"
      if ( !defined $node );

    my $value = expression( join ' ', @step );
    tellUser(
      "Setting the attribute '$attr' of element with id '$id'" . " to '$value'",
      1,
      " "
    );
    $node->setAttribute( $attr, $value );
    return;
  }
  if ( $command eq "my" ) {
    my $verbatim = 0;
    my $key      = expression( shift @step );
    if ( !defined $key ) {
      return if ($trying);
      die "autosvg: my: User variable key expected";
    }
    my $word = shift @step;
    if ( defined $word ) {
      if ( $word eq "verbatim" ) {
        $verbatim = 1;
      }
      elsif ( $word eq '\verbatim' ) {
        unshift @step, 'verbatim';
      }
      else {
        unshift @step, $word;
      }
    }
    my $value = join ' ', @step;
    if ( !$verbatim ) {
      $value = expression($value);
    }
    $userData{$key}     = $value;
    $userDataUsed{$key} = 0;
    tellUser( "Set user variable '$key' to '$value'", 1, " " );
    return;
  }
  if ( $command eq "print" ) {
    if ( !@step ) {
      print "\n";
      return;
    }
    my $stepCopy = $wholeStep;
    $stepCopy =~ s/^[ ]*print[ ]{1}//;

    # remove the newline
    if ( $stepCopy =~ m/\n$/m ) {
      $stepCopy = substr $stepCopy, 0, -1;
    }

    my $word = $step[0];

    my $inline = 0;
    if ( $word eq "inline" ) {
      $inline = 1;

      # Ignore 'inline' keyword
      shift @step;
      $stepCopy =~ s/inline[ ]?//;
    }
    elsif ( $word eq "\\inline" ) {
      $step[0] = "inline";
      $stepCopy =~ s/\\inline/inline/;
    }

    if ($inline) {
      print expression($stepCopy);
    }
    else {
      say expression($stepCopy);
    }
    return;
  }
  if ( $command eq "export" ) {
    my $outputName = expression( join ' ', @step );
    my $state      = $document->toFile( $outputName, 0 );
    die "autosvg: export: Writing to file unsuccessful: $outputName\n"
      if ( !defined $state );
    tellUser( "Saved the Document to $outputName", 1, " " );
    return;
  }
  if ( $command eq "if" ) {
    my $if   = 0;
    my $word = shift @step;

    if ( $word eq "set" ) {
      my $name = shift @step;
      if ( defined $userData{$name} ) {
        $userDataUsed{$name} = 1;
        tellUser( "\nVariable $name is set", -1 );
        $if = 1;
      }
    }
    else {
      unshift @step, $word;
      $tellUserGlobIndent++;
      $if = evalStep("eval @step");
      $tellUserGlobIndent--;
    }
    if ( $if != 1 ) {
      tellUser( "If condition not met", 1, "" );
      for ( $s++ ; $s < @steps ; $s++ ) {
        my @step = split ' ', $steps[$s];
        if ( defined $step[0] and $step[0] eq "done" ) {
          last;
        }
      }
      return;
    }
    tellUser( "If condition met", 1, "" );
    $tellUserGlobIndent++;
    tellUser( "\nIf block started", -1 );
    for ( $s++ ; $s < @steps ; $s++ ) {
      my @step = split ' ', $steps[$s];
      if ( defined $step[0] and $step[0] eq "done" ) {
        last;
      }
      evalStep( $steps[$s] );
    }
    tellUser( "\nIf block ended", 0, "" );
    $tellUserGlobIndent--;
    return;
  }
  if ( $command eq "data" ) {
    my $dataPath = expression( shift @step );

    @userColumnNames = ();
    my $start = 0;
    my $end;
    my $dataSeparator = ',';
    my $removeQuotes  = 1;

    my %keywords = (
      'as'           => 1,
      'from'         => 1,
      'to'           => 1,
      'sep'          => 1,
      'separated'    => 1,
      'clear'        => 1,
      'rmq'          => 1,
      'removequotes' => 1,
      'lq'           => 1,
      'leavequotes'  => 1
    );
    my %escapes = (
      '\as'           => 'as',
      '\from'         => 'from',
      '\to'           => 'to',
      '\sep'          => 'sep',
      '\separated'    => 'separated',
      '\clear'        => 'clear',
      '\rmq'          => 'rmq',
      '\removequotes' => 'removequotes',
      '\lq'           => 'lq',
      '\leavequotes'  => 'leavequotes'
    );

    my $args           = '';
    my $currentKeyword = undef;
    while ( my $arg = shift @step ) {
      if ( $arg eq "as" ) {
        @userColumnNames = ();
        my $i = 0;
        while ( my $nameAlias = shift @step ) {
          if ( defined $keywords{$nameAlias} ) {
            unshift @step, $nameAlias;    # put the keyword back
            last;
          }
          $nameAlias = $escapes{$nameAlias} if ( defined $escapes{$nameAlias} );
          push @userColumnNames, $nameAlias;
          $i++;
        }
        next;
      }
      if ( $arg eq "from" ) {
        shift @step if ( defined $step[0] and $step[0] eq "line" );
        $start = shift @step;
        die "autosvg: data: No 'from' value found after keyword\n"
          if ( !defined $start );
        $start--;
        next;
      }
      if ( $arg eq "to" ) {
        shift @step if ( defined $step[0] and $step[0] eq "line" );
        $end = shift @step;
        die "autosvg: data: No 'end' value found after keyword\n"
          if ( !defined $end );
        $end--;
        next;
      }
      if ( $arg eq "sep" or $arg eq "separated" ) {
        shift @step if ( $step[0] eq "by" );
        $dataSeparator = shift @step;
        die "autosvg: data: No data separator found after keyword\n"
          if ( !defined $dataSeparator );
        $dataSeparator = "by" if ( $dataSeparator eq '\by' );
        next;
      }
      if ( $arg eq "lq" or $arg eq "leavequotes" ) {
        $removeQuotes = 0;
        next;
      }
      if ( $arg eq "rmq" or $arg eq "removequotes" ) {
        $removeQuotes = 1;
        next;
      }
      if ( $arg eq "clear" ) {
        $clear = 1;
        next;
      }
      die "autosvg: data: Keyword expected. Got: $arg\n";
    }

    die "autosvg: data: Failed opening data file: $dataPath\n"
      unless ( -f $dataPath );
    open my $dataHandle, $dataPath
      or die "autosvg: data: Data could not be data: $dataPath:\n$!\n";

    my @data;

    while ( my $line = <$dataHandle> ) {
      $line =~ s/\R\z//;
      push @data, $line;
    }
    close $dataHandle;

    $end = ( @data - 1 ) unless ( defined $end );

    die "autosvg: data: The 'from' value needs to be at least 1\n"
      if ( $start < 0 );
    die "autosvg: data: Specified 'from' value is larger than length"
      . " of the data file!\n"
      if ( $start >= @data );

    warnUser( "autosvg: data: Warning: Specified 'to' value larger "
        . "than length of data file! This value will be ignored.\n" )
      if ( $end >= @data );
    die "autosvg: data: Specified 'from' value is greater than "
      . " the specified 'to' value!"
      if ( $start > $end );

    my $fieldCount = ( length( $data[0] =~ s/[^\Q$dataSeparator\E]//rg ) + 1 );

    for ( my $i = 0 ; $i < $fieldCount ; $i++ ) {
      push @userColumnNames, $i if ( $i >= @userColumnNames );
    }

    my %lineData;
    for ( my $d = $start ; $d <= $end ; $d++ ) {
      %lineData = ();
      my @fields = split( $dataSeparator, $data[$d] );

      if ( substr( $data[$d], -1 ) eq $dataSeparator ) {

        # the last field is empty
        push @fields, '';
      }

      # skip empty lines
      next unless (@fields);

      die "autosvg: data: Number of fields differs across userDatas "
        . "in data file '$dataPath'. The first line had "
        . $fieldCount
        . " fields, while line "
        . ( $d + 1 ) . " had "
        . scalar @fields
        . " lines \n"
        unless ( @fields == $fieldCount );

      for ( my $f = 0 ; $f < @fields ; $f++ ) {
        $fields[$f] =~ s/^["']|["']$//g if ($removeQuotes);
        $lineData{ $userColumnNames[$f] } = $fields[$f];

     #say "\$lineData{$userColumnNames[$f]} -> $lineData{$userColumnNames[$f]}";
      }
      push @dbData, {%lineData};
    }
    return;
  }
  if ( $command eq "process" ) {
    my $getArgs = join ' ', @step;
    $clear = 0;    #default clear value
    evalStep("data $getArgs");

    # clear has been set by `data`, process can use it here
    my $dataPath = shift @step;
    $s++;          # Move to the next step after 'processing'

    my $rCount = 0;

    my $bodyStartPosition = $s;
    my $bodyEndPosition   = $s;

    $tellUserGlobIndent++;
    for my $r (@dbData) {
      $rCount++;
      tellUser(
        "\nLooping over $dataPath [" . ($rCount) . "/" . scalar @dbData . "]:",
        -1
      );

      # Prepare for executing steps on the next data userData
      $document = undef if ($clear);
      foreach my $key ( keys %{$r} ) {
        $userData{$key}     = ${$r}{$key};
        $userDataUsed{$key} = 0;
      }

      # %userData = %{$r};
      for ( ; $s < @steps ; $s++ ) {
        my @step = split ' ', $steps[$s];
        next if ( scalar(@step) == 0 );
        if ( $step[0] eq "done" ) {
          $bodyEndPosition = $s;
          $s = $bodyStartPosition;    # Move back to the top of the loop body
          last;
        }
        evalStep( $steps[$s] );
      }
    }
    tellUser( "\nFinished processing $dataPath", 0, "" );
    $s = $bodyEndPosition;    # End the loop at the 'done' keyword.
    $tellUserGlobIndent--;
    return;
  }
  if ( $command eq "read" ) {
    my $arg = expression( $step[0] );  # Check if read is used to read arguments

    my $flag = 0;
    if ( $arg eq "argument" || $arg eq "arguments" ) {
      $flag = 1;
      shift @step;                     # Get rid of the argument[s] keyword
    }
    elsif ( $arg eq "\argument" || $arg eq "\arguments" ) {
      $arg = substr $arg, 1, length($arg) - 1;
    }

    if ($flag) {
      my $i = 1;
      while ( my $varname = shift @step ) {
        $varname                = expression($varname);
        $userData{$varname}     = $userData{$i};
        $userDataUsed{$varname} = 0;
        $i++;
      }
      return;
    }

    while ( my $varname = shift @step ) {
      $varname = expression($varname);

      # Read data from stdin, without the newline
      $userData{$varname}     = substr <STDIN>, 0, -1;
      $userDataUsed{$varname} = 0;
    }
    return;
  }
  die "autosvg: Verb was not recognized: $command\n";
}

sub runCurrentSteps {
  my $line = $s;
  for ( ; $s < @steps ; $s++ ) {
    evalStep( $steps[$s] );
  }
  $s = $line;
}

runCurrentSteps();
