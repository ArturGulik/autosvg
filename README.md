# Autosvg

Automate working with SVGs.

Autosvg is an interpreter for [The Steps Language](https://gitlab.com/ArturGulik/autosvg/-/wikis/The-Steps-Language), a domain-specific scripting language used to describe how svg files should be created, processed and prepared for printing.

It allows you to create files in bulk using content from a database or automatically fill a document with data you provide interactively. With Autosvg the possibilities are endless.

# Installation

The program is available as a single script. Clone it and run it.

You may need to install Perl dependencies, especially [XML::LibXML](https://metacpan.org/pod/XML::LibXML).

```bash
git clone https://gitlab.com/ArturGulik/autosvg.git
```

# Documentation
For Autosvg usage information see:
```bash
autosvg --help
```
For information on the language that you can use to write Autosvg scripts please refer to:

### [The Steps Language Documentation](https://gitlab.com/ArturGulik/autosvg/-/wikis/The-Steps-Language)


# Licensing & Copyright
Autosvg is released under the [GNU GPLv3](LICENSE).

Copyright (C) 2022-2024 Artur Gulik